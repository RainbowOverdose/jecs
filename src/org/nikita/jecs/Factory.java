package org.nikita.jecs;

import java.util.concurrent.atomic.AtomicInteger;

public class Factory {
	
	//esta es una clase que aisla la creacion de las entidades a�adiendo los componentes basicos necesarios a varios templates de entidad
	//quizas crear un xml para perfiles genericos
	//tipo animales.xml para crear un animal que contenga las caracteristicas basicas para componentes de un animal(?)
	
	private static AtomicInteger ai = new AtomicInteger();
	
	public static Entity nuevoEnemigo() {
		Entity enemigo = new Entity(ai.getAndIncrement());
		//se agregan las siguientes componentes a trav�s del gestor de entidades
		//el enemigo necesita una componente de movimiento
		//script para buscar caminos
		//posicion
		//graficos
		//colision
		
		//Aqui existe la posibilidad de cargar algunos recursos y asignarlos de manera pseudo/aleatoria
		return enemigo;
	}

	public static Entity jugador() {
		Entity jugador = new Entity(ai.getAndIncrement());
		//se agregan las siguientes componentes a trav�s del gestor de entidades
		//script para buscar el camino
		//posicion
		//control
		//camara
		//graficos
		//colision
		return jugador;
	}
	
}

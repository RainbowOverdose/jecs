package org.nikita.jecs;

public interface Component {

	public String getComponentType ();
	public int componentCode(); // must to be a 2 power
	
}

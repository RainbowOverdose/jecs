package org.nikita.jecs;

public interface Systems {
	
	public boolean addEntity();
	public boolean delEntity();
	public boolean deactivateEntity();

	abstract void refresh();
	//se debe de poder modificar la coleccion a trav�s del gestor de entidades en todo momento

}

package org.nikita.jecs.componentes;

import org.nikita.jecs.Component;

public class PositionComponent implements Component {

	double x;
	double y;
	
	public PositionComponent (int x, int y) {
		this.x = x; 
		this.y = y;
	}
	
	@Override
	public String getComponentType() {
		return "Posicion";
	}

	@Override
	public int componentCode() {
		return 4;
	}

}

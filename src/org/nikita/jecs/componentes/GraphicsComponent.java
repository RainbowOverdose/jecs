package org.nikita.jecs.componentes;

import org.nikita.jecs.Component;

public class GraphicsComponent implements Component {

	@Override
	public String getComponentType() {
		return "Graficos";
	}

	@Override
	public int componentCode() {
		return 2;
	}

}

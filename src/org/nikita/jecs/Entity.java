package org.nikita.jecs;

import java.util.ArrayList;
import java.util.Observable;

// Maybe implementingting an Obervale pattern by myself would be a better option
public class Entity extends Observable implements Component{
	

	protected ArrayList<Component> componentes;
	//there should be all the methods to manage all te components
	protected int id;
	
	public Entity(int id) {
		componentes = new ArrayList<Component>();
		this.id = id;
	}
	
	public String getComponentType() {
		return this.getClass().getSimpleName();
	}
	
	public int getId() {
		return id;
	}

	@Override
	public int componentCode() {
		return 1;//1 is the entity code 2^0 special
	}
}
